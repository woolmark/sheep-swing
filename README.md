# Sheep for Swing

Sheep for Swing is an implementation sheep/woolmark for Java Swing.

# Feature

 - Run and jump the sheep
 - Jumped sheep is counted

# Logic

## Canvas Size

The default canvas size is `120x120`. This application is not enable resizing.
The image is scaled by integral multiplication if the canvas size is enable resizing.

## Color

These colors are one of better color set.

 - Sky : RGB(100, 255, 100)
 - Ground : RGB(150, 150, 255)

It will be changed if the platform is not supported RGB888.

## Images

The images should be drawn by the following image resources.
You maybe draw by yourself, if drawing image function does not exist on implementation platform.

### Sheep

![sheep00](http://woolmark.bitbucket.org/css3/sheep00.png)

![sheep01](http://woolmark.bitbucket.org/css3/sheep01.png)

### Fence

![fence](http://woolmark.bitbucket.org/css3/background.png)

## Ground

### Height

Ground height should be less than the fence height.

## Fence

### Position

The fence is on bottom and horizontal center.

## Sheep

### Run and Jump

The sheep runs from right to left on the ground, and jumps when a sheep goes over the fence.

### Animation

Sheep image flips two images frame by frame.
The images are a stretch and not a stretch. It's always stretch if a sheep is jumping.

### Append

The sheep append is started when user presses the left mouse button.
And the sheep append is stopped when user releases the left mouse button.

The left mouse button is replaced CENTER key or display touch,
if there is not the mouse user interface.

The appended sheep runs away when it moves to left edge.

## Counter

The sheep counter is on left upper on the canvas.
It counts up when a sheep jumps over the fence.

The number is loaded when the application is launched,
and is saved when the application is finished.
If the application does not have any storage to save the data, that function may not be implemented.

# License

WTFPL
