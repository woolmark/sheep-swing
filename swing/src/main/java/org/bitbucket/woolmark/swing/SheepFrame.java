package org.bitbucket.woolmark.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * SheepFrame.
 */
public class SheepFrame extends JFrame {

    private SheepCanvas canvas = new SheepCanvas();

    public SheepFrame() {

        setSize(120, 120);

        add(canvas, BorderLayout.CENTER);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                canvas.start();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                canvas.stop();
            }
        });

        MouseAdapter mouseAdapter = new MouseAdapter() {
            private MouseEvent start;

            @Override
            public void mousePressed(final MouseEvent me) {
                if (me.getClickCount() == 2) {
                    start = me;
                }
            }

            @Override
            public void mouseReleased(final MouseEvent me) {
                start = null;
            }

            @Override
            public void mouseDragged(final MouseEvent me) {
                if (start == null) {
                    return;
                }
                Point eventLocationOnScreen = me.getLocationOnScreen();
                Window window = SwingUtilities.windowForComponent(me.getComponent());
                window.setLocation(eventLocationOnScreen.x - start.getX(),
                        eventLocationOnScreen.y - start.getY());

            }
        };
        canvas.addMouseListener(mouseAdapter);
        canvas.addMouseMotionListener(mouseAdapter);

    }

}
